import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculadoraService {

  constructor() { }

  divide(dividendo, divisor) {
    if (divisor === 0) {
      return 'Não Existe divisão por zero!';
    }
    return dividendo / divisor;
  }
}
