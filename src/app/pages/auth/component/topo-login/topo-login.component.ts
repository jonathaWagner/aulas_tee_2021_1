import { Component, Input, OnInit } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'topo-login',
  templateUrl: './topo-login.component.html',
  styleUrls: ['./topo-login.component.scss'],
})
export class TopoLoginComponent implements OnInit {
  @Input() title;
  constructor() { }

  ngOnInit() {}

}
