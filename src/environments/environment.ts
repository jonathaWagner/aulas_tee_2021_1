// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCr_UcuFFiLYknUxWWQYWgQqnOBTdQmyao',
    authDomain: 'controle-jw.firebaseapp.com',
    projectId: 'controle-jw',
    storageBucket: 'controle-jw.appspot.com',
    messagingSenderId: '661730642507',
    appId: '1:661730642507:web:3cf452df6b9ea3b7fe1bce',
    measurementId: 'G-C4GRHMXSVH'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
